package eltex.bulat.lab8.controller;

import eltex.bulat.lab8.orders.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.UUID;

public interface OrderRepository extends JpaRepository<Order, UUID> {
}

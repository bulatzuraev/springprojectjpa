package eltex.bulat.lab8.controller;


import eltex.bulat.lab8.orders.Order;
import eltex.bulat.lab8.orders.Orders;
import eltex.bulat.lab8.orders.OrdersGenerator;
import eltex.bulat.lab8.orders.ShoppingCart;
import eltex.bulat.lab8.product.AbstractProduct;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/")
public class OrdersController {
    @Autowired
    private Service service;
    private static final Logger logger = LogManager.getLogger(OrdersController.class);
    @RequestMapping(method=RequestMethod.GET)
    public Object commands(
            @RequestParam(value="command") String command,
            @RequestParam(required=false,value="order_id",defaultValue = "null") String orderId,
            @RequestParam(required=false,value="cart_id",defaultValue = "null") String cartId
    ){
        if ("save".equals(command)){
            logger.info("saving");
            service.save();
            return 0;
        }
        if ("readAll".equals(command)){
            logger.info("reading orders");
            return service.getAllOrders();
        }
        if ("readById".equals(command)&&!"null".equals(orderId)){
            logger.info("reading order");
            return service.getById(orderId);
        }
        if("addToCart".equals(command)&&!"null".equals(cartId)){
            logger.info("adding to cart");
            return service.addToCart(cartId);
        }
        if ("delById".equals(command)&&!"null".equals(orderId)){
            logger.info("deleting");
            return service.delById(orderId);
        }
        return 3;
    }
}

@Component
class Service{
    private ShoppingCartRepository shoppingCartRepository;
    private OrderRepository orderRepository;
    @Autowired
    Service(OrderRepository orderRepository, ShoppingCartRepository shoppingCartRepository){
        this.orderRepository = orderRepository;
        this.shoppingCartRepository = shoppingCartRepository;
    }
    public void save(){
        orderRepository.save(OrdersGenerator.generateOrder());
        orderRepository.save(OrdersGenerator.generateOrder());
        orderRepository.save(OrdersGenerator.generateOrder());
        orderRepository.save(OrdersGenerator.generateOrder());
    }
    public List<Order> getAllOrders(){
        return orderRepository.findAll();
    }
    public Order getById(String id){
        return orderRepository.findById(UUID.fromString(id)).get();
    }
    public UUID addToCart(String id){
        Iterator<Order> orderIt = orderRepository.findAll().iterator();
        UUID productId = null;
/*        while (orderIt.hasNext()){
            Order temp = orderIt.next();
            if (temp.shoppingCart.getId().equals(UUID.fromString(id))){
                AbstractProduct product = OrdersGenerator.generateProduct("Tea");
                temp.shoppingCart.add(product);
                productId = product.getObjectId();
                orderRepository.save(temp);
            }
        }*/
        Optional<ShoppingCart<AbstractProduct>> optional = shoppingCartRepository.findById(UUID.fromString(id));
        if (optional.isPresent()){
            ShoppingCart<AbstractProduct> temp = optional.get();
            AbstractProduct product = OrdersGenerator.generateProduct("Tea");
            temp.add(product);
            productId = product.getObjectId();
            shoppingCartRepository.save(temp);
        }
        return productId;
    }
    public int delById(String id){
        UUID uuid;
        try{
            uuid = UUID.fromString(id);
            orderRepository.deleteById(uuid);
        }catch(IllegalArgumentException e){
            return 1;
        }catch(EmptyResultDataAccessException e){
            return 1;
        }

        return 0;
    }
}
package eltex.bulat.lab8.controller;

import eltex.bulat.lab8.orders.ShoppingCart;
import eltex.bulat.lab8.product.AbstractProduct;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ShoppingCartRepository extends JpaRepository<ShoppingCart<AbstractProduct>, UUID> {
}

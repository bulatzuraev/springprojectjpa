package eltex.bulat.lab8.product;

public interface ICRUDAction {
    void create();
    void read();
    void update();
    void delete();
}

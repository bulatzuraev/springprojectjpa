package eltex.bulat.lab8.product;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Random;
import java.util.Scanner;
import java.util.UUID;
@Entity
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class AbstractProduct implements ICRUDAction, Serializable {

    static int productCounter;
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID objectId;
    @Column(name="name")
    private String name;
    @Column(name="cost")
    private int cost;
    @Column(name="firm")
    private String firm;
    @Column(name="country")
    private String country;
    protected AbstractProduct(){
        ++productCounter;
        this.objectId = UUID.randomUUID();
    }

    public static int getProductCounter() {
        return productCounter;
    }

    public UUID getObjectId() {
        return objectId;
    }

    public void setObjectId(UUID objectId) {
        this.objectId = objectId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getFirm() {
        return firm;
    }

    public void setFirm(String firm) {
        this.firm = firm;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
    public void create(){
        Random rand = new Random();
        int i = rand.nextInt(100);
        setName("name" + i);
        setCost(rand.nextInt(100));
        i = rand.nextInt(100);
        setFirm("firm" + i);
        i = rand.nextInt(100);
        setCountry("country" + i);
    }
    public void read(){
        System.out.println(this);
    }
    public void update() throws NumberFormatException{
            Scanner scan = new Scanner(System.in);
            System.out.println("Enter name");
            setName(scan.nextLine());
            System.out.println("Enter cost");
            setCost(Integer.parseInt(scan.nextLine()));
            System.out.println("Enter firm");
            setFirm(scan.nextLine());
            System.out.println("Enter country");
            setCountry(scan.nextLine());
    }
    public void delete(){
        setObjectId(null);
        setName(null);
        setCost(0);
        setFirm(null);
        setCountry(null);
        --productCounter;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("Product counter: ");
        sb.append(productCounter);
        sb.append(System.getProperty("line.separator"));
        sb.append("Object ID: ");
        sb.append(objectId);
        sb.append(System.getProperty("line.separator"));
        sb.append("Name: ");
        sb.append(name);
        sb.append(System.getProperty("line.separator"));
        sb.append("Cost: ");
        sb.append(cost);
        sb.append(System.getProperty("line.separator"));
        sb.append("Firm: ");
        sb.append(firm);
        sb.append(System.getProperty("line.separator"));
        sb.append("Country: ");
        sb.append(country);
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }
}
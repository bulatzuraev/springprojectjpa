package eltex.bulat.lab8.product;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Random;
import java.util.Scanner;
@Entity
public class Tea extends AbstractProduct {
    @Column(name="pack_type")
    private String packType;
    public Tea(){
        super();
        create();
    }

    private String getPackType() {
        return packType;
    }

    private void setPackType(String packType) {
        this.packType = packType;
    }

    public void create(){
        super.create();
        Random rand = new Random();
        int i = rand.nextInt(100);
        setPackType("pack" + i);
    }
    public void read(){
        System.out.println(this);
    }
    public void update(){
        try{
            super.update();
            Scanner scan = new Scanner(System.in);
            System.out.println("Enter pack type");
            setPackType(scan.nextLine());
        }catch(NumberFormatException e){
            System.out.println("Invalid cost, try again");
            update();
        }

    }
    public void delete(){
        super.delete();
        setObjectId(null);
        setPackType(null);
        --productCounter;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Pack type: ");
        sb.append(packType);
        sb.append(System.getProperty("line.separator"));
        return super.toString() + sb.toString();
    }
}

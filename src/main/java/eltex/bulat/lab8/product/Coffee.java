package eltex.bulat.lab8.product;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.Random;
import java.util.Scanner;
@Entity
public class Coffee extends AbstractProduct {
    @Column(name="beans_type")
    private String beansType;
    public Coffee(){
        super();
        create();
    }

    private String getBeansType() {
        return beansType;
    }

    private void setBeansType(String beansType) {
        this.beansType = beansType;
    }

    public void create(){
        super.create();
        Random rand = new Random();
        int i = rand.nextInt(100);
        setBeansType("beans" + i);
    }
    public void read(){
        System.out.println(this);
    }
    public void update(){
        try{
            super.update();
            Scanner scan = new Scanner(System.in);
            System.out.println("Enter beans type");
            setBeansType(scan.nextLine());
        }catch(NumberFormatException e){
            System.out.println("Invalid cost, try again");
            update();
        }

    }
    public void delete(){
        super.delete();
        setObjectId(null);
        setBeansType(null);
        --productCounter;
    }
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("Beans type: ");
        sb.append(beansType);
        sb.append(System.getProperty("line.separator"));
        return super.toString() + sb.toString();
    }
}

package eltex.bulat.lab8.orders;

import eltex.bulat.lab8.product.AbstractProduct;
import eltex.bulat.lab8.product.Coffee;
import eltex.bulat.lab8.product.Tea;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class OrdersGenerator implements Runnable {
    Orders<Order> orders;
    int sleepTime;
    public OrdersGenerator(Orders<Order> orders){
        this.orders = orders;
        sleepTime = 2;
    }
    public OrdersGenerator(Orders<Order> orders, int sleepTime){
        this.orders = orders;
        this.sleepTime = sleepTime;
    }
    public static AbstractProduct generateProduct(String product){
        if ("Tea".equals(product)){
            return new Tea();
        } else if ("Coffee".equals(product)){
            return new Coffee();
        }
        return null;
    }
    private static ShoppingCart<AbstractProduct> generateShoppingCart(){
        String[] productType = {"Tea","Coffee"};
        ShoppingCart<AbstractProduct> cart = new ShoppingCart();
        Random rand = new Random();
        for (int i = 0; i < 5; ++i){
            cart.add(generateProduct(productType[rand.nextInt(2)]));
        }
        return cart;
    }
    private static Credential generateCredential(){
        Random rand = new Random();
        int i = rand.nextInt(100);
        String firstName = "firstName" + i;
        String secondName = "secondName" + i;
        String middleName = "middleName" + i;
        return new Credential(firstName, secondName, middleName);
    }
    public static Order generateOrder(){
        Random rand = new Random();
        return new Order(rand.nextInt(10),generateShoppingCart(),generateCredential());
    }
    public void run(){
        Random rand = new Random();
        while (true){
            try {
                synchronized (orders){
                    orders.makePurchase(new Order(rand.nextInt(10),generateShoppingCart(),generateCredential()));
                    System.out.println("makePurchase, sleeptime="+sleepTime);
                }
                TimeUnit.SECONDS.sleep(sleepTime);
            } catch (InterruptedException e){
                System.err.println("Interrupted");
                break;
            }

        }
    }
}

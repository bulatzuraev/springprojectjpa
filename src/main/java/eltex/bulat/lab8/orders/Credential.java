package eltex.bulat.lab8.orders;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.util.UUID;
@Entity
public class Credential implements Serializable {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;
    @Column(name = "firstName")
    private String firstName;
    @Column(name = "secondName")
    private String secondName;
    @Column(name = "middleName")
    private String middleName;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public Credential(){
    }

    public Credential(String firstName, String secondName, String middleName) {
        id = UUID.randomUUID();
        this.firstName = firstName;
        this.secondName = secondName;
        this.middleName = middleName;
    }
    public void read(){
        System.out.println(this);
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("ID: ");
        sb.append(id);
        sb.append(System.getProperty("line.separator"));
        sb.append("First name: ");
        sb.append(firstName);
        sb.append(System.getProperty("line.separator"));
        sb.append("Second name: ");
        sb.append(secondName);
        sb.append(System.getProperty("line.separator"));
        sb.append("Middle name: ");
        sb.append(middleName);
        sb.append(System.getProperty("line.separator"));
        return sb.toString();
    }
}

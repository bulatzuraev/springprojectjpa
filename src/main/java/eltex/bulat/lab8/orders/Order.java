package eltex.bulat.lab8.orders;

import eltex.bulat.lab8.product.AbstractProduct;

import javax.persistence.*;
import java.io.Serializable;
import java.time.Duration;
import java.time.Instant;
import java.util.UUID;

@Entity
@Table(name = "ord")
public class Order implements Serializable {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;
    @Enumerated(EnumType.STRING)
    private Status status;
    @Column(name = "creationTime")
    private Instant creationTime;
    @Column(name = "awaitingTime")
    private Duration awaitingTime;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "shoppingCart_id")
    public ShoppingCart<AbstractProduct> shoppingCart;
    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name = "credential_id")
    public Credential credential;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Instant getCreationTime() {
        return creationTime;
    }

    public Duration getAwaitingTime() {
        return awaitingTime;
    }

    public void setAwaitingTime(Duration awaitingTime) {
        this.awaitingTime = awaitingTime;
    }
    public Order(){

    }
    public Order(int awaitingTime, ShoppingCart shoppingCart, Credential credential){
        id = UUID.randomUUID();
        this.status = Status.awaiting;
        this.creationTime = Instant.now();
        this.awaitingTime = Duration.ofSeconds(awaitingTime);
        this.shoppingCart = shoppingCart;
        this.credential = credential;
    }
    public void read(){
        System.out.println(this);
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append("ID: ");
        sb.append(id);
        sb.append(System.getProperty("line.separator"));
        sb.append("Status: ");
        sb.append(status);
        sb.append(System.getProperty("line.separator"));
        sb.append("Creation time: ");
        sb.append(creationTime);
        sb.append(System.getProperty("line.separator"));
        sb.append("Awaiting time: ");
        sb.append(awaitingTime);
        sb.append(System.getProperty("line.separator"));
        return sb.toString() + shoppingCart.toString() + credential.toString();
    }
    public void changeStatus(){
        status = Status.processed;
    }
}

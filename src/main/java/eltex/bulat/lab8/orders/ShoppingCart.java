package eltex.bulat.lab8.orders;

import eltex.bulat.lab8.product.AbstractProduct;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
@Entity
public class ShoppingCart <T extends AbstractProduct> implements Serializable {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;
    @ManyToMany(targetEntity=AbstractProduct.class, cascade = {CascadeType.ALL})
    @JoinColumn(name="product_id")
    private List<T> products;
    public UUID getId() {
        return id;
    }

    public List<T> getProducts() {
        return products;
    }

    public ShoppingCart(){
        products = new ArrayList<>();
        id = UUID.randomUUID();
    }
    public boolean add(T product){
        return products.add(product);

    }
    public void read(){
        System.out.println(this);
    }
    public T search(UUID id){
        for (T product : products){
            if (product.getObjectId().equals(id)){
                return product;
            }
        }
        return null;
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (T product : products){
            sb.append(product);
        }
        return sb.toString();
    }
}

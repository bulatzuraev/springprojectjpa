package eltex.bulat.lab8.orders;

import org.springframework.stereotype.Component;

import javax.persistence.*;
import java.time.Instant;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Entity
@Component
public class Orders <T extends Order> {
    @Id
    @Column(columnDefinition = "BINARY(16)")
    private UUID id;
    @OneToMany(targetEntity = Order.class, cascade = {CascadeType.ALL})
    @JoinColumn(name="order_id")
    private List<T> orders;
    public List<T> getOrders() {
        return orders;
    }

    public Orders(){
        id = UUID.randomUUID();
        orders = new LinkedList<>();
    }
    public void makePurchase(T order){
        orders.add(order);
    }
    public void checkUp(){
        Iterator<T> it = orders.iterator();
        while (it.hasNext()){
            T order = it.next();
            Instant awaiting = order.getCreationTime().plus(order.getAwaitingTime());
            if (order.getStatus() == Status.processed || Instant.now().compareTo(awaiting) > 0){
                it.remove();
            }
        }
    }
    public void read(){
        System.out.println(this);
    }
    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (T order : orders){
            sb.append(order);
        }
        return sb.toString();
    }
}
